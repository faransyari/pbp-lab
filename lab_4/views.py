from django.shortcuts import render
from lab_2.views import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

def index(request):
    index = Note.objects.all()
    response = {'notes': index}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    return render(request, "lab4_form.html")

def note_list(request):
    index = Note.objects.all()
    response = {'notes': index}
    return render(request, 'lab4_note_list.html', response)
