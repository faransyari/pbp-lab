import 'package:flutter/material.dart';

class homepage extends StatelessWidget {
  @override
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: Column(
          children: [
            Text(
              '\nWelcome to HeyDoc',
              style: optionStyle,
            ),
            Text('-Your personal Covid - 19 assistant -',
                style: TextStyle(color: Colors.black.withOpacity(0.8))),
            Text(
              '\n\nIn this pandemic era, HeyDoc is a platform to assist you. With our assistance, we strive to make your daily life easier through this pandemic.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14),
            ),
            Text("\nIndonesia's Covid - 19 Cases Right Now :",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Text(
                "\nPositive : 4250855\nHospitalized : 9018\nDeath Cases : 143659\nHealed : 4098178",
                style: TextStyle(fontSize: 14)),
            Text('\n\nOur Services\n',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: const <Widget>[
                Icon(
                  Icons.perm_device_information,
                  color: Colors.blue,
                  size: 36.0,
                ),
                Icon(
                  Icons.medication,
                  color: Colors.blue,
                  size: 36.0,
                ),
                Icon(
                  Icons.local_hospital,
                  color: Colors.blue,
                  size: 36.0,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}