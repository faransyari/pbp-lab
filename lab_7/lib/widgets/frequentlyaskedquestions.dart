import 'package:flutter/material.dart';

class faq extends StatelessWidget {
  @override
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  Widget build(BuildContext context) {
    return new Scaffold(body: new Center(child: Container(
      padding: EdgeInsets.all(36),
      child:
          // ignore: prefer_const_literals_to_create_immutables
          Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Positioned(
            top: 0,
            height: 250,
            width: 250,
            child: Container(
              child: Text(
                'Frequently Asked Questions',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Positioned(
            top: 90,
            height: 250,
            width: 250,
            child: Container(
              child: Text(
                "I don't know what medicine to use?",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          Positioned(
            top: 150,
            height: 250,
            width: 250,
            child: Container(
              child: Text(
                "If you don't know or not sure what medicine to buy, you can visit the consultation section. This section will have you talk to professional doctors with your problem. The doctors should be able to recommend the right medicine for you.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
            ),
          ),
          Positioned(
            top: 270,
            height: 250,
            width: 250,
            child: Container(
              child: Text(
                "Can I change my password?",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          Positioned(
            top: 300,
            height: 250,
            width: 250,
            child: Container(
              child: Text(
                "For now, the password of your account cannot be changed. The only way is to create a new account",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
            ),
          ),
        ],
      ),
    ),));
  }
}
