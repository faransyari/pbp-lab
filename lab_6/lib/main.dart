/// Flutter code sample for BottomNavigationBar

// This example shows a [BottomNavigationBar] as it is used within a [Scaffold]
// widget. The [BottomNavigationBar] has three [BottomNavigationBarItem]
// widgets, which means it defaults to [BottomNavigationBarType.fixed], and
// the [currentIndex] is set to index 0. The selected item is
// amber. The `_onItemTapped` function changes the selected item's index
// and displays a corresponding message in the center of the [Scaffold].

// ignore_for_file: prefer_final_fields, prefer_const_constructors

import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';


void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'HeyDoc';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MyStatefulWidget(),
      theme: ThemeData(
        canvasColor: Colors.white,
      )
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    Container(
      padding: EdgeInsets.all(36), 
      child:
      Column(
        children: [
          Text(
        'Welcome to HeyDoc',
        style: optionStyle,
      ),
      Text(
        '-Your personal Covid - 19 assistant -',
        style: TextStyle(color: Colors.black.withOpacity(0.8))
      ),
      Text(
        '\n\nIn this pandemic era, HeyDoc is a platform to assist you. With our assistance, we strive to make your daily life easier through this pandemic.',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 14),
      ),
      Text("\nIndonesia's Covid - 19 Cases Right Now :",
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
      ),
      Text("\nPositive : 4250855\nHospitalized : 9018\nDeath Cases : 143659\nHealed : 4098178",
      style: TextStyle(fontSize: 14)),
      Text('\n\nOur Services\n',
      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
      ),
      Row(
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: const <Widget>[
    Icon(
      Icons.perm_device_information,
      color: Colors.blue,
      size: 24.0,
      semanticLabel: 'Text to announce in accessibility modes',
    ),
    Icon(
      Icons.medication,
      color: Colors.blue,
      size: 30.0,
    ),
    Icon(
      Icons.local_hospital,
      color: Colors.blue,
      size: 36.0,
    ),
  ],
)     
        ],
      ),
    ),
    Container(padding: EdgeInsets.all(36), child: 
    Column(
      children: [
        Text(
      'Suggestions',
      style: optionStyle,
    ),
    Padding(
          padding: EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter your name',
            ),
          ),
        ),
    Padding(
          padding: EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Message',
            ),
          ),
        ),
    TextButton(
  style: ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(0, 173, 181, 1)),
  ),
  onPressed: () { },
  child: Text('Submit'),
),
      ],
    )
    ,),
    Container(padding: EdgeInsets.all(36),child: 
    // ignore: prefer_const_literals_to_create_immutables
    Stack(
      alignment: Alignment.topCenter,
  children: <Widget>[
    Positioned(
      top: 0,
      height:250,
      width: 250,
      child: Container(
        child: Text(
          'Frequently Asked Questions',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ),
    ),
    Positioned(
      top: 90,
      height:250,
      width: 250,
      child: Container(
        child: Text(
          "I don't know what medicine to use?",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20),
        ),
      ),
    ),
    Positioned(
      top: 150,
      height:250,
      width: 250,
      child: Container(
        child: Text(
          "If you don't know or not sure what medicine to buy, you can visit the consultation section. This section will have you talk to professional doctors with your problem. The doctors should be able to recommend the right medicine for you.",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14),
        ),
      ),
    ),
    Positioned(
      top: 270,
      height:250,
      width: 250,
      child: Container(
        child: Text(
          "Can I change my password?",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20),
        ),
      ),
    ),
    Positioned(
      top: 300,
      height:250,
      width: 250,
      child: Container(
        child: Text(
          "For now, the password of your account cannot be changed. The only way is to create a new account",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14),
        ),
      ),
    ),
 ],
),
    
    )
    
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HeyDoc'),
        backgroundColor: Color(0xFF00ADB5),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            label: 'Suggestions',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.question_answer),
            label: 'FAQ',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color.fromRGBO(0, 173, 181, 1),
        onTap: _onItemTapped,
      ),
    );
  }
}
