from django.shortcuts import render
from lab_2.models import Note

def index(request):
    index = Note.objects.all()
    response = {'notes': index}
    return render(request, 'lab2.html', response)