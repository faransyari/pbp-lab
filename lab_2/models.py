from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=128)
    note_from = models.CharField(max_length=128)
    title = models.CharField(max_length=128)
    message = models.TextField(max_length=512)
