from lab_2.models import Note
from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('json',json),
    path('xml',xml)
]