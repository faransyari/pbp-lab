from django.shortcuts import render
from .models import Note
from django.core import serializers
from django.http.response import HttpResponse

# Create your views here.
def index(request):
    index = Note.objects.all()
    response = {'notes': index}
    return render(request, 'lab2.html', response)

def xml(request):
    xml_data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(xml_data, content_type="application/xml")

def json(request):
    json_data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(json_data, content_type="application/json")