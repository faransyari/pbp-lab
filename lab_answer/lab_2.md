**(Question 1) Apakah perbedaan antara JSON dan XML?**
The difference between JSON and XML:
- JSON has a JavaScript Object Notation, XML has an Extensible markup language
- JSON is based on JavaScript language, XML is derived from SGML
- JSON supports array, XML doesn't support array
- JSON doesn't support comments, XML supports comments

**(Question 2) Apakah perbedaan antara HTML dan XML?**
The difference between HTML and XML:
- HTML shows data and the stucture of a website, XML stores and transfers data
- HTML is not case sensitive, XML is case sensitive
- HTML white spaces are not preserved, XML is capable of preserving white spaces
- HTML is static in nature, XML is dynamic in nature

**Result from /lab-2 page:**
```html
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Notes</title>
  </head>
  <body>
    <table>
      <tbody><tr>
        <th>To</th>
        <th>From</th>
        <th>Title</th>
        <th>Message</th>
      </tr>
      
      <tr>
        <td>Adrian</td>
        <td>Landi</td>
        <td>Adrian Nice</td>
        <td>Adrian is a nice guy</td>
      </tr>
      
      <tr>
        <td>Tito</td>
        <td>Landi</td>
        <td>To my bestiest bestie</td>
        <td>You are my bestie tito</td>
      </tr>
      
      <tr>
        <td>Zain</td>
        <td>Landi</td>
        <td>Zain you suck</td>
        <td>I dont need you zain &gt;:(</td>
      </tr>
      
    </tbody></table>
</body></html>
```

**Result from /lab-2/json page:**

```json
[
    {"model": "lab_2.note",
    "pk": 1, 
    "fields": {
        "to": "Adrian", 
        "note_from": "Landi", 
        "title": "Adrian Nice", 
        "message": "Adrian is a nice guy"
        }
    }, 
    {
        "model": "lab_2.note", 
        "pk": 3, 
        "fields": {"to": "Tito", 
        "note_from": "Landi", 
        "title": "To my bestiest bestie", 
        "message": "You are my bestie tito"
        }
    }, 
    {
        "model": "lab_2.note", 
        "pk": 4, 
        "fields": {"to": "Zain", 
        "note_from": "Landi", 
        "title": "Zain you suck", 
        "message": "I dont need you zain >:("
        }
    }
]

```

**Result from /lab-2/xml page:**

```xml
<django-objects version="1.0">
  <object model="lab_2.note" pk="1">
    <field name="to" type="CharField">Adrian</field>
    <field name="note_from" type="CharField">Landi</field>
    <field name="title" type="CharField">Adrian Nice</field>
    <field name="message" type="CharField">Adrian is a nice guy</field>
  </object>
  <object model="lab_2.note" pk="3">
    <field name="to" type="CharField">Tito</field>
    <field name="note_from" type="CharField">Landi</field>
    <field name="title" type="CharField">To my bestiest bestie</field>
    <field name="message" type="CharField">You are my bestie tito</field>
  </object>
  <object model="lab_2.note" pk="4">
    <field name="to" type="CharField">Zain</field>
    <field name="note_from" type="CharField">Landi</field>
    <field name="title" type="CharField">Zain you suck</field>
    <field name="message" type="CharField">I dont need you zain >:(</field>
  </object>
</django-objects>

```
